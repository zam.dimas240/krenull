import styles from "../../../styles/cartItem.module.scss"
import React, {useState} from 'react'

const CartItem = ({func, content, index, move}) => {
  return(
    <button disabled={content || !move} onClick={func} className={styles.cartItem__item}>
      {content}
    </button>
  )
}

export default CartItem
