import CartItem from "./CartItem/CartItem";
import styles from "../../styles/cart.module.scss"
import React, {useState, useEffect} from 'react'
import gameWinner from './gameWinner'
import {useDispatch, useSelector} from "react-redux";
import {addBotCount, addMoveBot, addMoveGame, addMoveUser, addUserCount, clearGame} from "../../redux/actions";
import {Observable} from "rxjs";

const Cart = ({}) => {
  const [move, setMove] = useState(true)
  const dispatch = useDispatch()
  const arrayGame = useSelector(state => state.gameArray.gameArray)
  const winner = gameWinner(arrayGame)

  function xBot() {
    if (winner === 'X') {
      dispatch(addUserCount())
      dispatch(clearGame())
      return;
    } else if (winner === 'O') {
      dispatch(addBotCount())
      dispatch(clearGame())
      return;
    }
  }

  useEffect(() => {
    xBot();
  })

  // const stream$ = new Observable(observer => {
  //   observer.next('first')
  //   observer.next('led')
  // })
  //
  // stream$.subscribe(val => console.log(val))
  const click = (index) => {
    dispatch(addMoveUser(index))
    setMove(false)
    // setTimeout(() => {
      setMove(true)
      dispatch(addMoveBot())
      console.log(arrayGame)
    // },1500)
  }
  return(
    <div className={styles.cart}>
      <div className={styles.cart__items}>
        {arrayGame.map((el, index) =>
          <CartItem key={index} content={el} id={index} func={() => click(index)} move={move}/>
        )}
      </div>
      <div className={styles.move}>
        {move &&
          <h2>Ходит X</h2>
        }
        {!move &&
          <h2>Ходит O</h2>
        }
      </div>
      <div className={styles.button}>
        <button className={styles.button__item} onClick={() => dispatch(clearGame())}>Очистка</button>
      </div>
    </div>
  )
}

export default Cart
