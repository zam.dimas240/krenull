import styles from './CartMenu.module.scss'
import {useSelector} from "react-redux";

const CartMenu = ({}) => {
  const userCount = useSelector(state => state.counter.userWin)
  const botCount = useSelector(state => state.counter.botWin)

  return (
    <div>
      <div className={styles.menu__container}>
        <div className={styles.menu__count_block}>
          <h2>Счет:</h2>
          <div className={styles.menu__count_blocks}>
            <div className={styles.menu__count}>
              <h2>Бот: {botCount}</h2>
            </div>
            <div className={styles.menu__count}>
              <h2>Пользователь: {userCount}</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CartMenu
