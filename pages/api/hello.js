// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const data = []

export default function handler(req, res) {
  console.log(req.type)
  if (req.type === 'GET') {
    return res.json(data);
  } else {
    data.push({ player: 1, win: true })
  }
  res.status(200).json({ data: data })
}
