import '../styles/globals.css'
import {createStore} from "redux";
import {Provider} from "react-redux";
import {store} from "../redux/rootReducer";
import React from "react";

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
