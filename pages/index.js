import styles from '../styles/Home.module.css'
import Cart from "../components/Cart/Cart";
import CartMenu from "../components/CartMenu/CartMenu";

export default function Home() {
  return (
    <div className={styles.body}>
      <CartMenu/>
      <Cart/>
    </div>
  )
}
