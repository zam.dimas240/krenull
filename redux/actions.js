import {ADD_BOT_COUNT, ADD_MOVE_BOT, ADD_MOVE_GAME, ADD_MOVE_USER, ADD_USER_COUNT, CLEAR_GAME} from "./types";

export function addUserCount() {
 return {
   type: ADD_USER_COUNT
 }
}

export function addMoveUser(id) {
 return {
   type: ADD_MOVE_USER,
   payload: id
 }
}
export function addMoveBot() {
 return {
   type: ADD_MOVE_BOT,
 }
}

export function addBotCount() {
 return {
   type: ADD_BOT_COUNT
 }
}

export function clearGame() {
 return {
   type: CLEAR_GAME
 }
}
