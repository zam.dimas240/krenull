import {applyMiddleware, combineReducers, createStore} from "redux";
import {ADD_BOT_COUNT, ADD_MOVE_BOT, ADD_MOVE_USER, ADD_USER_COUNT, CLEAR_GAME} from "./types";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

const defaultCounterWinner = {
  userWin: 0,
  botWin: 0
}


function counterWinnerReducer(state= defaultCounterWinner, action) {
 switch (action.type) {
   case ADD_USER_COUNT:
      return {...state, userWin: state.userWin + 1}
   case ADD_BOT_COUNT:
      return {...state, botWin: state.botWin + 1}
   default:
     return state
 }
}

const defaultGameArray = {
  gameArray: Array(9).fill(null)
}

function randomIndex(arr) {
  const random = Math.floor(Math.random() * 9)
    if (arr[random] === null) {
      arr[random] = "O"
      console.log(random);
      return arr
    } else {
      console.log('eche', arr)
      randomIndex(arr)
    }
  return arr
}

function gameArrayReducer(state = defaultGameArray, action) {
  switch (action.type) {
    case CLEAR_GAME:
      return {...state, gameArray: Array(9).fill(null)}
    case ADD_MOVE_USER:
      return {...state, gameArray: state.gameArray.map((item, index) => {
          if (index === action.payload && item === null) {
            item = "X"
          }
          return item
        })}
    case ADD_MOVE_BOT:
      return {...state, gameArray: randomIndex(state.gameArray) }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  counter: counterWinnerReducer,
  gameArray: gameArrayReducer
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
